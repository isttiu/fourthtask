import Foundation

enum tip1{
    
    case num1(Int)
    case word(String)
    
}

enum tip2{
    
    case num2(Double)
    case num3(Float)
    
}

enum sex{
    
    case female(String)
    case male(String)
    
}

enum age{
    
    case young(Int)
    case middle(Int)
    case old(Int)
    
}

enum experience{
    
    case little(Int)
    case average(Int)
    case great(Int)
    
}

enum rainbowColor{
    
    case red, orange, yellow, green, blue, darkblue, purple
    
}

var mass = ["sun", "apple", "banana", "sky", "sea", "box", "juice"]

func association(associate: rainbowColor){
    
    switch associate{
        
    case .red:
        print("red \(mass[0])")
    case .orange:
        print("orange \(mass[6])")
    case .yellow:
        print("yellow \(mass[2])")
    case .green:
        print("green \(mass[1])")
    case .blue:
        print("blue \(mass[3])")
    case .darkblue:
        print("darkblue \(mass[4])")
    case .purple:
        print("purple \(mass[5])")
    }
    
}
association(associate: rainbowColor.red)
association(associate: rainbowColor.orange)
association(associate: rainbowColor.yellow)
association(associate: rainbowColor.green)
association(associate: rainbowColor.blue)
association(associate: rainbowColor.darkblue)
association(associate: rainbowColor.purple)

print("")
enum Score{
    
    case two
    case three
    case four
    case five
    
    
}

var students = ["Гриша", "Влад", "Регина", "Света"]

func magazine(scores: Score){
    
    switch scores{
        
    case .two:
        print("\(students[0]) - 2")
    case .three:
        print("\(students[1]) - 3")
    case .four:
        print("\(students[2]) - 4")
    case .five:
        print("\(students[3]) - 5")
    }
    
}
magazine(scores: Score.two)
magazine(scores: Score.three)
magazine(scores: Score.four)
magazine(scores: Score.five)

print("")

enum auto: CaseIterable{
    
    case audi, bmw, mitsubishi, toyota, lada
    
}

for i in auto.allCases{
    
    print("В гараже стоит \(i)")
    
}
